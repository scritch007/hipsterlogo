var leftOffset = 200;
var width = 300;
var topOffset = 20;
var leftCenter = width + leftOffset;



function Logo(){
	this.badge = new Logo.Badge();
	this.text = new Logo.Text("text");

	this.description_top = new Logo.Description("short desc top");
	this.description_top.set_direction(Logo.Description.Top);

	this.description_bottom = new Logo.Description("short desc bottom");
	this.description_bottom.set_direction(Logo.Description.Bottom);

	this.banner = new Logo.Banner();

	this.set_badge = function(badge){
		this.badge = badge;
	}
	this.set_text = function(text){
		this.text = text;
	}
	this.set_description_bottom = function(description){
		this.description_bottom = description;
		description.set_direction(Logo.Description.Bottom);
	}

	this.set_description_top = function(description){
		this.description_top = description;
		description.set_direction(Logo.Description.Top);
	}

	this.set_banner = function(banner){
		this.banner = banner;
	}

	this.set_extra_left = function(extra){
		this.extra_left = extra;
		extra.set_direction(Logo.Extra.Left);
	}

	this.set_extra_right = function(extra){
		this.extra_right = extra;
		extra.set_direction(Logo.Extra.Right);
	}

	this.extra_left = new Logo.Extra("left");
	this.set_extra_left(this.extra_left);

	this.extra_right = new Logo.Extra("right");
	this.set_extra_right(this.extra_right);

	this.draw = function(canvas){
		var ctx = canvas.getContext("2d");
		ctx.save();

		// Use the identity matrix while clearing the canvas
		ctx.setTransform(1, 0, 0, 1, 0, 0);
		ctx.clearRect(0, 0, canvas.width, canvas.height);

		// Restore the transform
		ctx.restore();

		this.badge.draw(ctx);
		this.banner.draw(ctx);
		this.text.draw(ctx);
		this.description_bottom.draw(ctx);
		this.description_top.draw(ctx);
		this.extra_left.draw(ctx);
		this.extra_right.draw(ctx);
	}

}

Logo.Badge = function(){
	this.draw = function(ctx){
		ctx.save();
		ctx.beginPath();
        ctx.arc(leftCenter, width/2 + topOffset, width/2, 0, Math.PI*2, false);
        ctx.closePath();
        ctx.fill();

		ctx.strokeStyle = 'blue';
		ctx.lineWidth = 4;
		ctx.beginPath();
		ctx.arc(leftCenter, width/2 + topOffset, (width - (width/10)) /2, 0, Math.PI*2, false);
		ctx.stroke();
        ctx.closePath();
        ctx.restore();
    }
}

Logo.Banner = function(){
	this.draw = function(ctx){
		var delta = width / 10;
		ctx.save();
		ctx.strokeStyle = "black";
		ctx.beginPath();

		ctx.moveTo(leftCenter - (width/2 + delta), width / 2 - (width / 10) + topOffset);
		ctx.lineTo(leftCenter + (width/2 + delta), width / 2 - (width / 10) + topOffset);
		ctx.lineTo(leftCenter + (width/2 + delta), width / 2 + (width / 10) + topOffset);
		ctx.lineTo(leftCenter - (width/2 + delta), width / 2 + (width / 10) + topOffset);
		ctx.closePath();
		//ctx.stroke();
		ctx.fillStyle = "red";
		ctx.fill();

		ctx.beginPath();
		ctx.moveTo(leftCenter - (width/2 + delta), width / 2 - (width / 10) + topOffset);
		ctx.lineTo(leftCenter - (width/2), width / 2 + topOffset);
		ctx.lineTo(leftCenter - (width/2 + delta), width / 2 + (width / 10) + topOffset);
		ctx.closePath();
		ctx.fillStyle = "white";
		ctx.fill();

		ctx.beginPath();
		ctx.moveTo(leftCenter + (width/2 + delta), width / 2 - (width / 10) + topOffset);
		ctx.lineTo(leftCenter + width/2, width / 2 + topOffset);
		ctx.lineTo(leftCenter + (width/2 + delta), width / 2 + (width / 10) + topOffset);
		ctx.closePath();
		ctx.fillStyle = "white";
		ctx.fill();
		ctx.restore();
	}
}

Logo.Text = function(text, font){

	this.text = text;
	this.size = "18pt";

	this.draw = function(ctx){
		ctx.save();
		ctx.beginPath();
		ctx.font = this.size + ' ' + this.font;
		ctx.textAlign = "center";
      	ctx.fillStyle = '#333';
		//ctx.fillText(this.text, leftCenter - (this.text.length / 2 * 8), width / 2 + topOffset);
		ctx.fillText(this.text, leftCenter, width/2 + topOffset);
		ctx.closePath();
		ctx.restore();
	}
}

Logo.Description = function(description){
	this.text = description;
	this.font = "Calibri";
	this.size = "10pt";

	this.set_direction = function(direction){
		this.direction = direction;
	}
	this.draw = function(ctx){
		ctx.save();
    	ctx.font = this.size + ' ' + this.font;
      	ctx.textAlign = 'center';
      	ctx.fillStyle = 'blue';
      	ctx.strokeStyle = 'blue';
      	ctx.lineWidth = 4;

        var len = this.text.length
        var s;

        var angle;
        if(Logo.Description.Top == this.direction)
        {
        	angle = Math.PI * 0.8;
        }else{
        	angle = -Math.PI * 0.8;
        }
        ctx.save();
        ctx.translate(leftCenter, width / 2 + topOffset);

		if(Logo.Description.Top == this.direction){
        	ctx.rotate( -1 * angle / 2);
        	ctx.rotate( -1 * (angle / len) / 2);
        }else{
        	ctx.rotate( -1 * angle / 2);
        	ctx.rotate( -1 * (angle / len) / 2);
        }

        for(var n = 0; n < len; n++) {
          if (Logo.Description.Top == this.direction)
          {
          	ctx.rotate( 1 * angle / len);
          }else{
          	ctx.rotate( angle / len);
          }
          ctx.save();
          if (Logo.Description.Top == this.direction)
          {
          	ctx.translate(0, -1 * (width/2 - width/10));
          }else{
          	ctx.translate(0, 1 * (width/2 - width/10));
          }
          s = this.text[n];
          ctx.fillText(s, 0, 0);
          ctx.restore();
        }
        ctx.restore();
        ctx.restore();
	}
}

Logo.Description.Bottom = 0;
Logo.Description.Top = 1;

Logo.Extra = function(text){
	this.text = text;
	this.font = "Calibri";
	this.size = "10pt";

	this.set_direction = function(direction){
		this.direction = direction;
	}
	this.draw = function(ctx){
		ctx.save();
		ctx.font = this.size + ' ' + this.font;
		if (this.direction == Logo.Extra.Left){
			ctx.textAlign = "right";
			ctx.fillText(this.text, leftCenter - width / 2 - width / 10, width/2 + topOffset);
		}else{
			ctx.textAlign = "left";
			ctx.fillText(this.text, leftCenter + width / 2 + width / 10, width/2 + topOffset);
		}
		ctx.restore();
	}
}

Logo.Extra.Left = 0;
Logo.Extra.Right = 1;