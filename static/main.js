function create_list(element){


	element.onclick = function(){
		var div = document.createElement("div");
		div.style.position = "absolute";
		div.style.top = element.offsetTop;
		div.style.left = element.offsetLeft;
		div.style.backgroundColor = "white";
		for(var i=0; i < font_list.length; i++){
			var line = document.createElement("p");
			var font_name = font_list[i];
			line.style.fontFamily = font_name;
			line.innerHTML = font_name;

			line.onclick = function(){
				element.style.fontFamily = this.innerHTML;
				element.innerHTML = this.innerHTML;
				div.style.display = "none";
				div.parentNode.removeChild(div);
				update_logo();
			}
			div.appendChild(line);
		}
		document.body.appendChild(div);
	}
	//element.innerHTML = font_list[0];
	//element.style.fontFamily = font_list[0];
}

var logo;

function update_logo(){
	var canva = document.getElementById("drawing");

	var tradeSize = document.getElementById("tradeSize");
	var tradeFont = document.getElementById("tradeFont");
	var trade     = document.getElementById("trade");

	var descriptionTopSize = document.getElementById("descriptionTopSize");
	var descriptionTopFont = document.getElementById("descriptionTopFont");
	var descriptionTop     = document.getElementById("descriptionTop");

	var descriptionBottomSize = document.getElementById("descriptionBottomSize");
	var descriptionBottomFont = document.getElementById("descriptionBottomFont");
	var descriptionBottom     = document.getElementById("descriptionBottom");

	var rightExtraSize = document.getElementById("rightExtraSize");
	var rightExtraFont = document.getElementById("rightExtraFont");
	var rightExtra     = document.getElementById("rightExtra");

	var leftExtraSize = document.getElementById("leftExtraSize");
	var leftExtraFont = document.getElementById("leftExtraFont");
	var leftExtra     = document.getElementById("leftExtra");

	logo.text.font = tradeFont.innerHTML;
	logo.text.text = trade.innerHTML;
	logo.text.size = tradeSize.innerHTML;

	logo.description_top.font = descriptionTopFont.innerHTML;
	logo.description_top.text = descriptionTop.innerHTML;
	logo.description_top.size = descriptionTopSize.innerHTML;

	logo.description_bottom.font = descriptionBottomFont.innerHTML;
	logo.description_bottom.text = descriptionBottom.innerHTML;
	logo.description_bottom.size = descriptionBottomSize.innerHTML;

	logo.extra_left.font = leftExtraFont.innerHTML;
	logo.extra_left.text = leftExtra.innerHTML;
	logo.extra_left.size = leftExtraSize.innerHTML;


	logo.extra_right.font = rightExtraFont.innerHTML;
	logo.extra_right.text = rightExtra.innerHTML;
	logo.extra_right.size = rightExtraSize.innerHTML;

	logo.draw(canva);
}

function update_text(event){
	var elementName = event.srcElement.id.substr(0,event.srcElement.id.length - 5);
	var text = document.getElementById(elementName);
	if ((event.keyCode==13) || (event.keyCode==27)){
		event.stopPropagation();
		event.srcElement.style.display = "none";
		text.style.display ="";
		return false;
	}

	text.innerHTML = event.srcElement.value;

	update_logo();
	return true;
}

function text_blur(event){
	var elementName = event.srcElement.id.substr(0,event.srcElement.id.length - 5);
	var text = document.getElementById(elementName);
	event.srcElement.style.display = "none";
	text.style.display ="";
}


function SetFontSize(elementName){
	var element = document.getElementById(elementName+"Range");
	var elementSize = document.getElementById(elementName+"Size");
	elementSize.innerHTML = element.value + "pt";
	update_logo();

}

var selectedElement = null;

function showInput(elementName){
	var element = document.getElementById(elementName);
	var elementInput = document.getElementById(elementName+"Input");
	elementInput.value = element.innerHTML;
	element.style.display = "none";
	elementInput.style.display = "";
	elementInput.focus();
	selectedElement = elementInput;
}

function init(){

	var canva = document.getElementById("drawing");
	logo = new Logo();

	var info = document.getElementById("info");

	//Add a tradmark entry
	tradeFont = document.getElementById("tradeFont");
	create_list(tradeFont);
	//var tradeRange = document.getElementById("tradeRange");
	//tradeRange.onchange = function(){
	//console.log(tradeRange);
	//};
	var descriptionTopFont = document.getElementById("descriptionTopFont");
	create_list(descriptionTopFont);

	var descriptionBottomFont = document.getElementById("descriptionBottomFont");
	create_list(descriptionBottomFont);

	var leftExtraFont = document.getElementById("leftExtraFont");
	create_list(leftExtraFont);

	var rightExtraFont = document.getElementById("rightExtraFont");
	create_list(rightExtraFont	);

	var button = document.getElementById("button");

	button.onclick = function(){
		update_logo();
	}

	update_logo();
}